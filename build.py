import argparse
import glob
import logging
import json
import subprocess as sp 
import sys

from pathlib import Path

ARCH_DIR = "arch/"
REQUIRED_KEYS = ["C_COMPILER", "CXX_COMPILER", "INCLUDE_DIR", "LIBRARY_DIR", "OUT_FILE", "ARCH"]

logging.basicConfig(format="%(asctime)s %(levelname)8s: %(message)s", level=logging.INFO)
log = logging.getLogger("")

config_list = glob.glob(ARCH_DIR + "*.json")
arch_list = [s[5:-5] for s in config_list]

log.debug(arch_list)

parser = argparse.ArgumentParser(description="Script that will build a toolchains(s) based on criteria")
command_group = parser.add_mutually_exclusive_group(required=True)
command_group.add_argument("--buildall", action='store_true')
command_group.add_argument("--build", choices=arch_list, nargs="+")

def validate_keys(config_file: str):
    error = False
    log.debug("Validating keys...")
    for key in REQUIRED_KEYS:
        log.debug("Checking if \"{}\" is in {}".format(key, config_file.keys()))
        if key not in config_file.keys():
            log.error("Missing {} field in {}".format(key, config_file))
            error = True
    if error:
        log.critical(f"Fix missing keys in {config_file}")
        sys.exit(-1)


def vaildate_values(config_file: str, config_path: str):
    error = False
    log.debug("Validating values of keys...")
    if Path(config_file["INCLUDE_DIR"]).is_dir() is False:
        if Path(config_file["INCLUDE_DIR"]).is_file():
            log.error("INCLUDE_DIR must be a directory, not a file: {}".format(config_file["INCLUDE_DIR"]))
        error = True
    if error:
        log.critical(f"Fix the INCLUDE_DIR path in {config_path}")
        sys.exit(-1)

    if Path(config_file["LIBRARY_DIR"]).is_dir() is False:
        if Path(config_file["LIBRARY_DIR"]).is_file():
            log.error("LIBRARY_DIR must be a directory, not a file: {}".format(config_file["INCLUDE_DIR"]))
        log.error("LIBRARY_DIR must be a directory: {}".format(config["LIBRARY_DIR"]))
        error = True
    if error:
        log.critical(f"Fix the LIBRARY_DIR path in {config_path}")
        sys.exit(-1)

    if Path(config_file["C_COMPILER"]).is_file() is False:
        if Path(config_file["C_COMPILER"]).is_dir():
            log.error("C_COMPILER must be a file, not a directory: {}".format(config_file["C_COMPILER"]))
        log.error("C_COMPILER should not be a directory.")
        error = True
        log.error("C_COMPILER is not a file: {}".format(config["C_COMPILER"]))
    if error:
        log.critical(f"Fix the C_COMPILER path in {config_path}")
        sys.exit(-1)

    if Path(config_file["CXX_COMPILER"]).is_file() is False:
        if Path(config["CXX_COMPILER"]).is_dir():
            log.error("CXX_COMPILER must be a file, not a directory: {}".format(config_file["CXX_COMPILER"]))
        log.error("CXX_COMPILER should not be a file: {}".format(config_file["CXX_COMPILER"]))
        error = True
    if error:
        log.critical(f"Fix the CXX_COMPILER path in {config_path}")
        sys.exit(-1)

    if Path(config_file["OUT_FILE"]) == "":
        log.error(f"Could not find an out_file name for the binary for {config_path}.")
        error = True
    if error:
        log.critical(f"Fix the OUT_FILE field in {config_path}.")
        sys.exit(-1)

    if Path(config_file["ARCH"]) == "":
        log.error(f"Could not find ARCH field in {config_path}.")
        error = True
    if error:
        log.critical(f"Fix the ARCH field in {config_path}.")
        sys.exit(-1)

def build_arch(config_path: str):
    # error = False
    if Path(config_path).is_file() is False:
        log.error(f"{config_path} does not exist.")
        sys.exit(-1)

    log.debug(f"Opening {config_path}")
    raw_config = open(config_path, 'r').read()
    try:
        config = json.loads(raw_config)
    except:
        log.error(f"Failed to open, read & parse {config_path}.")
        sys.exit(-1)

    # Input checks
    validate_keys(config) 
    vaildate_values(config, config_path)
    
    # Cmake commands
    log.info("Building for {}....".format(config["ARCH"]))
    cmake_compile_cmd = "cmake . -Bbuild -DCMAKE_C_COMPILER={} -DCMAKE_CXX_COMPILER={} -DCMAKE_INCLUDE_DIR={} -DCMAKE_LIBRARY_DIR={} ".format(config["C_COMPILER"], config["CXX_COMPILER"], config["INCLUDE_DIR"], config["LIBRARY_DIR"])
    cmake_build_cmd = "cmake --build build"
    mv_cmd = "mv build/hello bin/{}".format(config["OUT_FILE"])

    try:
        log.debug("Compiling...")
        proc = sp.run(cmake_compile_cmd.split(), stdout=sp.PIPE, stderr=sp.PIPE)
        log.debug("cmake compile return code: {}".format(proc.returncode))
        log.debug("cmake compile output: {}".format(proc.stdout))
        log.debug("cmake compile error output: {}".format(proc.stderr))
    except:
        log.error("Compilation failed: {}".format(proc.stderr))
        sys.exit(-1)
    try:
        log.debug("Building...")    
        proc = sp.run(cmake_build_cmd.split(), stdout=sp.PIPE, stderr=sp.PIPE)
        log.debug("cmake build return code: {}".format(proc.returncode))
        log.debug("cmake build output: {}".format(proc.stdout))
        log.debug("cmake build error output: {}".format(proc.stderr))
    except:
        log.error("Build process failed: {}".format(proc.stderr))
        sys.exit(-1)
    try:
        log.debug(f"Moving output binary to bin.")
        log.debug("out_file name: {}".format(config["OUT_FILE"]))
        proc = sp.run("mkdir -p bin".split(), stdout=sp.PIPE, stderr=sp.PIPE)
        proc = sp.run(mv_cmd.split(), stdout=sp.PIPE, stderr=sp.PIPE)
    except:
        log.error("Failed to move binary: {}".format(proc.stderr))
        log.error(proc.stdout)
        sys.exit(-1)

    return_code = proc.returncode
    output = proc.stdout
    err = proc.stderr

    log.debug(return_code)
    log.debug(output)
    log.debug(err)

        
args = parser.parse_args()
arch = str(args.build)

#Clean up
log.debug("Clearing out bin/ and build/ ...")
sp.run("rm -rf bin/ build/".split(), stdout=sp.PIPE, stderr=sp.PIPE)

if args.buildall:
    for config_file in config_list:
        build_arch(config_file)

elif len(arch) > 1:
    for a in args.build:
        config_file = ARCH_DIR + a + ".json"
        build_arch(config_file)
else:
    config_file = ARCH_DIR + arch + ".json"
    build_arch(config_file)
    