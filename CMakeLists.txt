cmake_minimum_required(VERSION "3.16.3")

project(toolchain_project)
message("Project: ${PROJECT_NAME}")

add_executable(hello hello_world.c adder.c)

include_directories(${PROJECT_SOURCE_DIR}/include)

set_target_properties( PROPERTIES LINK_FLAGS "-static -s")

#add_custom_command(TARGET hello POST_BUILD COMMAND mkdir -p ../bin COMMAND cp hello ../bin/)
